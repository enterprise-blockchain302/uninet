# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF-UNINETWORK_0/fabric-samples/bin" ] ; then
    PATH="/workspaces/HLF-UNINETWORK_0/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm genesis.block mychannel.tx
rm natuni-genesis.block natuni-channel.tx
rm -rf ../../channel-artifacts/*

#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/


# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=/workspaces/uninet/artifacts/channel/configtx.yaml


# Generate the genesis block for the University Consortium Orderer
configtxgen -profile NatuniOrdererGenesis -channelID ordererchannel -outputBlock natuni-genesis.block

# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx ./natuni-channel.tx -profile NatuniChannel -channelID natunichannel

configtxgen -outputAnchorPeersUpdate Org1Anchors.tx -profile NatuniChannel -channelID natunichannel -asOrg Org1
